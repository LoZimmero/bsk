package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;


public class FrameTest {

	@Test
	@Ignore //To avoid deleting it, because I can't edit it ;D
	public void test() throws Exception{
		fail("Not yet implemented");
	}
	
	@Test
	// User story 1
	public void testFrameConstructorInvalidFirst() throws BowlingException {
		Exception e = assertThrows(BowlingException.class, () -> {
			Frame f = new Frame(11,5);
		});
		assertEquals(e.getClass(), BowlingException.class);
		assertEquals(e.getMessage(), "Invalid value for firstThrow. It must be at least 0 and less than " + Frame.MAX_PINS);
	}
	
	@Test
	// User story 1
	public void testFrameConstructorInvalidSecond() throws BowlingException {
		Exception e = assertThrows(BowlingException.class, () -> {
			Frame f = new Frame(5,11);
		});
		assertEquals(e.getClass(), BowlingException.class);
		assertEquals(e.getMessage(), "Invalid value for secondThrow. It must be at least 0 and less than " + Frame.MAX_PINS);
	}
	
	@Test
	// User story 1
	public void testFrameConstructorInvalidCombo() throws BowlingException {
		Exception e = assertThrows(BowlingException.class, () -> {
			Frame f = new Frame(8,8);
		});
		assertEquals(e.getClass(), BowlingException.class);
		assertEquals(e.getMessage(), "Invalid value for firstThrow and secondThrow. Pins must be maximum " + Frame.MAX_PINS);
	}
	
	@Test
	// User story 1
	public void testFrameConstructorOK() throws BowlingException {
		Frame f = new Frame(5,5);
		assertEquals(1, 1); // Assertion to check that no exception is thrown. Tried with assertDoesNotThrow but looks like JUnit5
	}
	
	@Test
	// User story 1
	public void testFrameGetFirst() throws BowlingException {
		Frame f = new Frame(5,5);
		assertEquals(f.getFirstThrow(), 5);
	}
	
	@Test
	// User story 1
	public void testFrameGetSecond() throws BowlingException {
		Frame f = new Frame(5,5);
		assertEquals(f.getFirstThrow(), 5);
	}
	
	@Test
	// User story 2
	public void testGetScore() throws BowlingException {
		Frame f = new Frame(3,5);
		assertEquals(f.getScore(), 8);
	}
	
	@Test
	// User story 5
	public void testGetBonus() throws BowlingException {
		Frame f = new Frame(5,5);
		f.setBonus(5);
		assertEquals(f.getBonus(), 5);
	}
	
	@Test
	// User story 5
	public void testSetBonus() throws BowlingException {
		Frame f = new Frame(5,5);
		f.setBonus(10);
		assertEquals(f.getBonus(), 10);
	}
	
	@Test
	// User story 5
	public void testIsSpare() throws BowlingException {
		Frame f = new Frame(5,5);
		assertEquals(f.isSpare(), true);
		Frame f2 = new Frame(1, 2);
		assertEquals(f2.isSpare(), false);
	}
	
	@Test
	// User story 6
	public void testIsStrike() throws BowlingException {
		Frame f = new Frame(5,5);
		assertEquals(f.isStrike(), false);
		Frame f2 = new Frame(10, 0);
		assertEquals(f2.isStrike(), true);
		// Case with Frame(10, <value != 0>) throws exception. see "testFrameConstructorInvalidCombo"
	}

}
