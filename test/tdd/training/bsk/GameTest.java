package tdd.training.bsk;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;

import org.junit.Test;

public class GameTest {
	
	@Test
	// User story 3
	public void testGameConstructorOK() throws BowlingException {
		Game g = new Game();
		assertEquals(1, 1);
	}
	
	@Test
	// User story 3
	public void testAddFrame() throws BowlingException {
		Game g = new Game();
		assertEquals(g.getFrames().size(), 0);
		g.addFrame(new Frame(0, 2));
		assertEquals(g.getFrames().size(), 1);
	}
	
	@Test
	// User story 3
	public void testAddFrameTooMany() throws BowlingException {
		Game g = new Game();
		for (int i = 0; i < Game.MAX_FRAMES; i++) {
			g.addFrame(new Frame(0, i));
		}
		Exception e = assertThrows(BowlingException.class, () -> {
			g.addFrame(new Frame(0, 2));
		});
		assertNotNull(e);
		assertEquals(e.getClass(), BowlingException.class);
	}
	
	@Test
	// User story 3
	public void testGetFrameAt() throws BowlingException {
		Game g = new Game();
		Frame f = new Frame(0, 2);
		g.addFrame(f);
		assertEquals(g.getFrameAt(0), f);
	}
	
	@Test
	// User story 3
	public void testGetFrameAtInvalidIndex() throws BowlingException {
		Game g = new Game();
		Frame f = new Frame(0, 2);
		g.addFrame(f);
		Exception e = assertThrows(BowlingException.class, () -> {
			g.getFrameAt(-1);
		});
		assertEquals(e.getClass(), BowlingException.class);
	}
	
	@Test
	// User story 3
	public void testGetFrameAtHigherIndex() throws BowlingException {
		Game g = new Game();
		Frame f = new Frame(0, 2);
		g.addFrame(f);
		Exception e = assertThrows(BowlingException.class, () -> {
			g.getFrameAt(1);
		});
		assertEquals(e.getClass(), BowlingException.class);
	}
	
	@Test
	// User story 4
	public void testCalculateScore() throws BowlingException {
		Game g = new Game();
		for (int i = 0; i < Game.MAX_FRAMES - 1; i++) {
			g.addFrame(new Frame(0, i));
		}
		g.addFrame(new Frame(5,5)); // Test also score in case of spare
		assertEquals(g.calculateScore(), 46);
	}
	
	@Test
	// User story 6
	public void testCalculateScoreWithStrike() throws BowlingException {
		Game g = new Game();
		g.addFrame(new Frame(1, 0));
		g.addFrame(new Frame(1, 0));
		g.addFrame(new Frame(1, 0));
		g.addFrame(new Frame(10, 0)); // STRIKE! (Value should be 11)
		g.addFrame(new Frame(1, 0));
		g.addFrame(new Frame(1, 0));
		g.addFrame(new Frame(1, 0));
		g.addFrame(new Frame(1, 0));
		g.addFrame(new Frame(10, 0)); // STRIKE! (Value should be 12)
		g.addFrame(new Frame(1, 1));
		assertEquals(g.calculateScore(), 32);
	}
	
	@Test
	// User story 7
	public void testCalculateScoreWithStrikeSpare() throws BowlingException {
		Game g = new Game();
		g.addFrame(new Frame(10, 0)); // STRIKE! (Value should be 20)
		g.addFrame(new Frame(4, 6));
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6)); 
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(2, 6));
		assertEquals(g.calculateScore(), 103);
	}
	
	@Test
	// User story 8
	public void testCalculateScoreMultipleStrikes() throws BowlingException {
		Game g = new Game();
		g.addFrame(new Frame(10, 0)); // STRIKE! (Value should be 27)
		g.addFrame(new Frame(10, 0)); // STRIKE! (Value should be 19)
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6)); 
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(2, 6));
		assertEquals(g.calculateScore(), 112);
	}
	
	@Test
	// User story 9
	public void testCalculateScoreMultipleSpares() throws BowlingException {
		Game g = new Game();
		g.addFrame(new Frame(8, 2)); // SPARE! (Value should be 15)
		g.addFrame(new Frame(5, 5)); // SPARE! (Value should be 17)
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6)); 
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(2, 6));
		assertEquals(g.calculateScore(), 98);
	}
	
	@Test
	// User story 10
	public void testSetFirstBonusThrow() throws BowlingException {
		Game g = new Game();
		g.addFrame(new Frame(1, 5)); // SPARE! (Value should be 15)
		g.addFrame(new Frame(3, 6)); // SPARE! (Value should be 17)
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6)); 
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(2, 8));
		g.setFirstBonusThrow(7);
		assertEquals(g.calculateScore(), 90);
	}
	
	@Test
	// User story 10
	public void testSetFirstBonusThrowError() throws BowlingException {
		Game g = new Game();
		g.addFrame(new Frame(1, 5)); // SPARE! (Value should be 15)
		g.addFrame(new Frame(3, 6)); // SPARE! (Value should be 17)
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6)); 
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(2, 7));
		// Here should throw exception because last frame is not spare
		Exception e = assertThrows(BowlingException.class, () -> {
			g.setFirstBonusThrow(7);
		});
		assertEquals(e.getClass(), BowlingException.class);
	}
	
	@Test
	// User story 11
	public void testSetSecondBonusThrow() throws BowlingException {
		Game g = new Game();
		g.addFrame(new Frame(1, 5)); // SPARE! (Value should be 15)
		g.addFrame(new Frame(3, 6)); // SPARE! (Value should be 17)
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6)); 
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(10, 0));
		g.setFirstBonusThrow(7);
		g.setSecondBonusThrow(2);
		assertEquals(g.calculateScore(), 92);
	}
	
	@Test
	// User story 11
	public void testSetSecondBonusThrowError() throws BowlingException {
		Game g = new Game();
		g.addFrame(new Frame(1, 5)); // SPARE! (Value should be 15)
		g.addFrame(new Frame(3, 6)); // SPARE! (Value should be 17)
		g.addFrame(new Frame(7, 2));
		g.addFrame(new Frame(3, 6)); 
		g.addFrame(new Frame(4, 4));
		g.addFrame(new Frame(5, 3));
		g.addFrame(new Frame(3, 3));
		g.addFrame(new Frame(4, 5));
		g.addFrame(new Frame(8, 1));
		g.addFrame(new Frame(9, 1));
		g.setFirstBonusThrow(7);	// OK because last is Spare
		// Here an exception is thrown because last Frame is not a Strike
		Exception e = assertThrows(BowlingException.class, () -> {
			g.setSecondBonusThrow(2);
		});
		assertEquals(e.getClass(), BowlingException.class);
	}
	
	@Test
	// User story 12
	public void testBestScore() throws BowlingException {
		Game g = new Game();
		g.addFrame(new Frame(10, 0)); 
		g.addFrame(new Frame(10, 0)); 
		g.addFrame(new Frame(10, 0)); 
		g.addFrame(new Frame(10, 0)); 
		g.addFrame(new Frame(10, 0)); 
		g.addFrame(new Frame(10, 0)); 
		g.addFrame(new Frame(10, 0)); 
		g.addFrame(new Frame(10, 0)); 
		g.addFrame(new Frame(10, 0)); 
		g.addFrame(new Frame(10, 0)); 
		g.setFirstBonusThrow(10);
		g.setSecondBonusThrow(10);
		assertEquals(g.calculateScore(), 300);
	}

}
