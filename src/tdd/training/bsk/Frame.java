package tdd.training.bsk;

public class Frame {
	public static int MAX_PINS = 10;
	private int firstThrow;
	private int secondThrow;
	private int bonus = 0;

	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		if (firstThrow < 0 || firstThrow > MAX_PINS) throw new BowlingException("Invalid value for firstThrow. It must be at least 0 and less than " + Frame.MAX_PINS);
		if (secondThrow < 0 || secondThrow > MAX_PINS) throw new BowlingException("Invalid value for secondThrow. It must be at least 0 and less than " + Frame.MAX_PINS);
		if ((firstThrow + secondThrow) > MAX_PINS) throw new BowlingException("Invalid value for firstThrow and secondThrow. Pins must be maximum " + Frame.MAX_PINS);
		this.firstThrow = firstThrow;
		this.secondThrow = secondThrow;
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		// To be implemented
		return firstThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {
		// To be implemented
		return secondThrow;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		this.bonus = bonus;
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		return bonus;
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		// To be implemented
		return this.firstThrow + this.secondThrow + this.bonus;
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {
		return (firstThrow == 10 && secondThrow == 0);
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		return ((firstThrow + secondThrow) == 10);
	}

}
