package tdd.training.bsk;

import java.util.ArrayList;
import java.util.List;

public class Game {
	private List<Frame> frames;
	public static int MAX_FRAMES = 10;
	private int firstBonusThrow;
	private int secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		this.frames = new ArrayList<Frame>();
		this.firstBonusThrow = 0;
		this.secondBonusThrow = 0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if (frames.size() >= MAX_FRAMES) throw new BowlingException("There are already " + Game.MAX_FRAMES + " frames");
		frames.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if (index < 0 || index >= frames.size()) throw new BowlingException("Invalid index");
		return frames.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		if (!(frames.get(frames.size()-1)).isSpare()) throw new BowlingException("Cannot set first bonus throw if last Frame is not spare");
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		if (!(frames.get(frames.size()-1)).isStrike()) throw new BowlingException("Cannot set second bonus throw if last Frame is not strike");
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		for (int i = frames.size() - 1; i >= 0; i --) {
			Frame f = getFrameAt(i);
			// Handle spare case
			if (f.isSpare() && i < (frames.size() - 1)) {
				f.setBonus(getFrameAt(i+1).getFirstThrow());
			}
			// Handle strike case.
			// Strike has priority over spare
			if (f.isStrike()) {
				int bonusToSet = 0;
				if (i < (frames.size() - 1)) { // It's not last element
					Frame next = getFrameAt(i+1);
					if (next.isStrike()) {
						bonusToSet += 10;
						if (i < (frames.size() - 2)) { //It's not demi-last element
							next = getFrameAt(i+2);
							bonusToSet += next.getFirstThrow();
						} else if (i == (frames.size() - 2)) {
							// Particular case: demi-last element is strike
							// and last is strike
							bonusToSet += firstBonusThrow;
						}
					} else {
						bonusToSet += (next.getFirstThrow() + next.getSecondThrow());
					}
				}
				f.setBonus(bonusToSet);
			}
			int currScore = f.getScore();
			score += currScore;
		}
		/*
		for (int i = 0; i < frames.size(); i++) {
			Frame f = getFrameAt(i);
			score += f.getScore();
			if (f.isStrike()) {
				int j = i+1;
				if (j < frames.size()) {
					score += getFrameAt(j).getScore();
					j += 1;
					if (j < frames.size()) {
						score += getFrameAt(j).getScore();
					}
				}
				
			}
		}*/
		
		// Handle bonus score
		score += firstBonusThrow;
		score += secondBonusThrow;
		return score;
	}

	public List<Frame> getFrames() {
		return frames;
	}

	public void setFrames(List<Frame> frames) {
		this.frames = frames;
	}
	
	

}
